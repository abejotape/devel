window.iniciarGMap = function() {
    var styles = [{
        stylers: [{
            hue: "#12253c"
        }, {
            "saturation": -10
        }, {
            "lightness": 10
        }]
    }, {
        featureType: "road",
        elementType: "geometry",
        stylers: [{
            lightness: 10
        }, {
            visibility: "simplified"
        }]
    }, {
        featureType: "road",
        elementType: "labels",
        stylers: [{
            visibility: "on"
        }]
    }];

    var styledMap = new google.maps.StyledMapType(styles, {
        name: "M&Z"
    });

    // Edit Ubication
    var miUbicacion = new google.maps.LatLng(-12.0889562, -77.021457);
    // End Edi

    var mapOptions = {
        zoom: 15,
        center: miUbicacion,
        scrollwheel: false,
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
        }
    };
    var map = new google.maps.Map(document.getElementById('load-gmap'), mapOptions);

    var contentString =
        '<div id="gm--message" class="cleaner">' +
        '<a class="gm--capa" href="https://goo.gl/T7kL8t" target="_blank"><span><i class="ico1 material-icons ">insert_emoticon</i></span></a>' +
        '<div class="top cleaner"><div class="cleaner tac"><img src="images/general/mz.jpg" alt="Martínez Samaniego"></div></div></div>' +
        '</div>' +
        '</div>' +
        '<div class="cleaner tac"><strong>Martínez Samaniego</strong></div>'

    var infowindow = new google.maps.InfoWindow({
        content: contentString,
        Width: 300
    });
    var marker = new google.maps.Marker({
        position: miUbicacion,
        map: map,
        title: "Martínez Samaniego",
        icon: 'images/general/pin-direc.svg',
        animation: google.maps.Animation.DROP
    });
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map, marker);
    });
    map.mapTypes.set('map_style', styledMap);
    map.setMapTypeId('map_style');
}
var myance = (function($, undefined) {
    "use strict";
    var events, suscribeEvents, beforeCatchDom, catchDom, dom, afterCatchDom, fn, initialize, st;
    st = {
    };
    dom = {};
    beforeCatchDom = function() {

    };
    catchDom = function() {
    };
    afterCatchDom = function() {
        fn.owlCarruselArticulo()
        fn.owlCarruselTestimonio()
        fn.animatedText($('#tipoSolucion'))
    };
    suscribeEvents = function() {
        $('#menuMobile').on('click', events.abrirMenu)

        $('#nextA').on('click', function () {
            var owl = $('.owl__articulos');
            owl.trigger('next.owl.carousel', [500]);
        });
        $('#prevA').on('click', function () {
            var owl = $('.owl__articulos');
            owl.trigger('prev.owl.carousel', [500]);
        });

        $('#nextT').on('click', function () {
            var owl = $('.owl__testimonios');
            owl.trigger('next.owl.carousel', [500]);
        });
        $('#prevT').on('click', function () {
            var owl = $('.owl__testimonios');
            owl.trigger('prev.owl.carousel', [500]);
        });
    };
    events = {
        abrirMenu: function(){
            $('html').toggleClass('open')
        }
    };
    fn = {
        animatedText: function (element) {
           element.typeIt({
                
                typeSpeed: 30,
                autoStart: true,
                loop: false
            })
           
            .tiType('Contables,')
            .tiPause(400)
            .tiType(' Administrativas,')
            .tiPause(400)
            .tiType(' Digitales')
            .tiPause(400)
            .tiType(' y TI')
        },
        owlCarruselArticulo: function () {
            var owl = $('.owl__articulos');
            owl.owlCarousel({
                loop:true,
                margin:40,
                responsive:{
                    0:{
                        items:1
                    },
                    400:{
                        items:1
                    },
                    700:{
                        items:2
                    },
                    1000:{
                        items:3
                    },
                    1400:{
                        items:4
                    },
                    1800:{
                        items:5
                    }
                }
            })  
        },
        owlCarruselTestimonio: function () {
            var owl = $('.owl__testimonios');
            owl.owlCarousel({
                loop:true,
                margin:20,
                responsive:{
                    0:{
                        items:1
                    }
                }
            })  
        }
    };
    initialize = function() {
        beforeCatchDom();
        catchDom();
        afterCatchDom();
        suscribeEvents();        
    };
    return {
        init: initialize
    };
})(jQuery);
myance.init();